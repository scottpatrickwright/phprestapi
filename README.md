## Overview

This is a light weight starting point for creating a REST API using PHP. Although REST is not typically a common pattern for PHP development it provides a cleaner demarcation point between server side and client side code than the common scenario in PHP where server side code generates front end mark up directly.

For reference: http://coreymaynard.com/blog/creating-a-restful-api-with-php/

## Implementation

There are no dependencies on external libraries. Access to the .htaccess file is required as a URL re-write rule must be created that allows the api to capture and process any url which points to non-existent files or directories, then re-directing to api PHP file which explodes the url and extracts REST verbs, arguments and methods from the call and ultimately maps this information to methods in PHP and  returns JSON data or the appropriate error . The .htacces rule looks like:

<IfModule mod_rewrite.c>
RewriteEngine On
RewriteCond %{REQUEST_FILENAME} !-f
RewriteCond %{REQUEST_FILENAME} !-d
RewriteRule api/v1/(.*)$ api/v1/api.php?request=$1 [QSA,NC,L]
</IfModule>

## Motivation

LAMP servers are *very* widespread. It would be good to be have a low overhead, easy to use starting point that allows us to create web services using the established REST best practices with PHP.

## Installation

...